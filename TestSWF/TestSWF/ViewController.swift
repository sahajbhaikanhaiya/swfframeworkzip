//
//  ViewController.swift
//  TestSWF
//
//  Created by Singh on 2018-01-02.
//  Copyright © 2018 Singh. All rights reserved.
//

import UIKit
import SWFFramework
import Alamofire

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let registerDomainAction = RegisterDomain(name: "ProperTry1", retentionPeriodInDays: "10")
        let url = URL(string: Config.URL)!
        var request = URLRequest(url: url)
        request.httpMethod = HTTPMethod.post.rawValue
        request.addValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        
        request.addValue("com.amazonaws.swf.service.model.SimpleWorkflowService.RegisterDomain", forHTTPHeaderField: "X-Amz-Target")
        
        request.addValue("amz-1.0", forHTTPHeaderField: "Content-Encoding")
        
        request.httpBody = registerDomainAction.getRequestBody()
        
        guard let signedRequest = URLRequestSigner().sign(request: request, secretSigningKey: Config.secretKey, accessKeyId: Config.accessKeyID) else {
            print ("Some error occured!!")
            return }
        
        print(signedRequest.allHTTPHeaderFields)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

