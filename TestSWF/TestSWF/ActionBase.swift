//
//  ActionBase.swift
//  SimpleWorkflowIOS
//
//  Created by Singh on 2017-12-31.
//  Copyright © 2017 Singh. All rights reserved.
//

import Foundation
class ActionBase
{
    internal var json: [String:String]?
    
    init()
    {
        json = ["":""]
    }
    
    internal func getJSONData(json:[String:String]) -> Data
    {
        var jsonData: Data?
        do
        {
            jsonData = try JSONSerialization.data(withJSONObject: json , options: .prettyPrinted)
        }
        catch
        {
            print("Error occured while serialization of Request Body!")
        }
        
        return jsonData!
    }
    
    func getRequestBody() -> Data
    {
        let jsonData = getJSONData(json: json!)
        
        return jsonData
    }
}

