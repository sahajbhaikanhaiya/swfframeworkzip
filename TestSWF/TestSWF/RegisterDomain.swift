//
//  RegisterDomain.swift
//  TestSWF
//
//  Created by Singh on 2018-01-02.
//  Copyright © 2018 Singh. All rights reserved.
//
import Foundation

class RegisterDomain:ActionBase
{
    
    //Required : YES
    private var name: String?
    private var workflowExecutionRetentionPeriodInDays: String?
    
    //Required : NO
    private var description: String = ""
    
    
    init(name: String, retentionPeriodInDays: String)
    {
        super.init()
        self.name = name
        self.workflowExecutionRetentionPeriodInDays = retentionPeriodInDays
        self.json = [
            "name":name,
            "workflowExecutionRetentionPeriodInDays":workflowExecutionRetentionPeriodInDays!,
            "description":description
        ]
    }
    
    init(name: String, description: String, retentionPeriodInDays: String)
    {
        super.init()
        self.name = name
        self.description = description
        self.workflowExecutionRetentionPeriodInDays = retentionPeriodInDays
        self.json = [
            "name":name,
            "workflowExecutionRetentionPeriodInDays":workflowExecutionRetentionPeriodInDays!,
            "description":description
        ]
    }
    
}

