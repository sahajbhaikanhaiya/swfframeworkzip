Pod::Spec.new do |s|

    s.name                  = 'SWFFramework'

    s.version               = '0.1.2'

    s.summary               = 'SWFFramework. Pod to add framework to any project.'

    s.description           = "This is a private cocoapod framework for SWF Api Gateway calls."

    s.homepage              = 'https://github.com'

    s.license               = { :type => 'MIT', :file => 'LICENSE'}

    s.author                = { 'sahajbhaikanhaiya' => 'sahaj.bhaikanhaiya@gmail.com' }

    s.source                = { :http => 'https://bitbucket.org/sahajbhaikanhaiya/swfframeworkzip/raw/bbd0413a716f208180c17e2a0b4d28f6f5afc18b/SWFFramework.framework.zip', :flatten => true }

    s.ios.deployment_target = '9.0'

    s.frameworks = 'SWFFramework'

    s.vendored_frameworks = 'SWFFramework.framework'

    s.dependency 'Alamofire', '~> 4.5'

    s.dependency 'CryptoSwift'

end
